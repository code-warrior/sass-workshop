# Sass Workshop

This repository contains slides (generated from Markdown syntax to PDF via the Deckset app) for a lecture/workshop on Sass.

## Comments/Suggestions

If you find any mistakes, would like to contribute, or have any comments/suggestions…

1. Create an issue

2. Generate a pull request
